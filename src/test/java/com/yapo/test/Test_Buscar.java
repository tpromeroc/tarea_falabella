package com.yapo.test;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.yapo.config.BaseConfig;
import com.yapo.dataprovider.IntegratedDataProvider;
import com.yapo.entities.DatosBuscarFal;
import com.yapo.page.Buscar_Falabella;

public class Test_Buscar extends BaseConfig {
	@Test(description="Buscar Producto desde Json", dataProvider = "buscar", dataProviderClass = IntegratedDataProvider.class)
	public void test(DatosBuscarFal datos) {
	SoftAssert soft = new SoftAssert();	
	Buscar_Falabella buscar = new Buscar_Falabella(driver);
	
	soft.assertTrue(buscar.buscarFal(datos.getTexto()));
	buscar.buscarCerc(10000);
	soft.assertTrue(buscar.Numero(),"se cayó en numero"); 
	buscar.Anadir();
	buscar.VerCarrito();
	buscar.IrCompra();
	soft.assertTrue(buscar.Region(datos.getRegion()));
	soft.assertTrue(buscar.Comuna(datos.getComuna()));
	buscar.Continuar();
	soft.assertTrue(buscar.Calle(datos.getCalle()));
	soft.assertTrue(buscar.NumC(datos.getNumero()));
	buscar.Agregar();
	buscar.Finalizar();
	
	}
}