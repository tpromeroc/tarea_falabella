package com.yapo.page;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yapo.config.BaseConfig;

public class Buscar_Falabella extends BaseConfig {
	public Buscar_Falabella(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="/html/body/div[1]/nav/div[3]/div/div[1]/div/div[1]/div/input")
	public WebElement ingreso;
	@FindBy(xpath = "//li[@data-undefined-price][contains(@class,'price-0')]")
	public List<WebElement> arreglo;
	
	@FindBy(xpath = "//*[@id=\"__next\"]/div/section/div[1]/div[1]/div/div/div/div[2]/section[2]/div/div/div/div[4]/div/div[2]/div[1]")
	public WebElement Numero;
	
	@FindBy(xpath = "//*[@id=\"buttonForCustomers\"]/button")
	public WebElement Agregar;
	
	
	@FindBy(xpath = "//*[@id=\"linkButton\"]")
	public WebElement Carrito;

	@FindBy(xpath = "//*[@id=\"root\"]/div[2]/div[2]/div/form/div[2]/div[2]/div[2]/button")
	public WebElement Comprar;

	@FindBy(xpath = "//*[@id=\"region\"]")
	public WebElement Region;
	
	@FindBy(xpath = "//*[@id=\"comuna\"]")
	public WebElement Comuna;
	
	@FindBy(xpath = "//*[@id=\"fbra_checkoutRegionAndComuna\"]/div/section/section/form/div/div[3]/div/button")
	public WebElement Continuar;
	
	@FindBy(xpath = "//*[@id=\"calle\"]")
	public WebElement Calle;
	
	@FindBy(xpath = "//*[@id=\"streetNumber\"]")
	public WebElement Num;
	
	@FindBy(xpath = "//*[@id=\"fbra_checkoutDeliverAndCollect\"]/div/div/div[1]/div/div[2]/div/div/form/section/div/div[5]/button")
	public WebElement AgregarDire;
	
	@FindBy(xpath = "//*[@id=\"fbra_checkoutDeliveryActions\"]/div/div/span/div/div/div/div[2]/button")
	public WebElement Finaliza;
	
	
	public boolean buscarFal(String Texto) {
		try {
		WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.elementToBeClickable(ingreso));
		ingreso.sendKeys(Texto);
		ingreso.sendKeys(Keys.ENTER);
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		return false;
	}
}
	public void buscarCerc(int valorbuscar) {
		List<Integer> zapatillas = new ArrayList<Integer>();
		for (WebElement span: arreglo) {
			String preciolimpio = span.getAttribute("data-undefined-price").trim().replace(".","");
			Integer precio = Integer.parseInt(preciolimpio);
			zapatillas.add(precio);
		}
		Integer [] array= zapatillas.toArray(new Integer[0]);
		int preciocercano = masCercano(array, 10000);
		System.out.println(preciocercano);
		
		DecimalFormat formatear = new DecimalFormat("###,###");
		String stringCerc = String.valueOf(formatear.format(preciocercano));
		System.out.println(stringCerc);
		
		WebElement seleccionProduc = driver.findElement(By.xpath("//li[@data-undefined-price=\""+stringCerc+"\"][contains(@class,'price-0')]"));
		seleccionProduc.click();
	}
	
public static Integer masCercano(Integer[] valores, Integer num) {
		
        int cercano = 0;
        int diferencia = Integer.MAX_VALUE; //inicializado valor máximo de variable de tipo int
        for (int i = 0; i < valores.length; i++) {
            if (valores[i] == num) {
                return valores[i];
            } else {
                if(Math.abs(valores[i]-num)<diferencia){
                    cercano=valores[i];
                    diferencia = Math.abs(valores[i]-num);
                }
            }
        }
        return cercano;
    }
	
public void Anadir() {
	Agregar.click();
}

public boolean Numero() {
	try {
		Thread.sleep(5000);
	Numero.click();
	return true;
	} catch (Exception e) {
		e.printStackTrace();
		return false;
	}
}

public void VerCarrito() {
	Carrito.click();
}

public void IrCompra() {
	Comprar.click();
}
public boolean Region (String DatoRegion) {
	try {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		wait.until(ExpectedConditions.elementToBeClickable(Region));
		Select selectRegion = new Select(Region);
		selectRegion.selectByVisibleText(DatoRegion);
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		return false;
	}
}

public boolean Comuna (String DatoComuna) {
	try {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		wait.until(ExpectedConditions.elementToBeClickable(Comuna));
		Select selectComuna = new Select(Comuna);
		selectComuna.selectByVisibleText(DatoComuna);
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		return false;
	}
}

public void Continuar() {
	Continuar.click();
}

public boolean Calle (String calle) {
	try {
		
		Calle.sendKeys(calle);
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		return false;
	}
}

public boolean NumC (String numero) {
	try {
		
		Num.sendKeys(numero);
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		return false;
	}
}

public void Agregar() {
	AgregarDire.click();
}

public void Finalizar() {
	Finaliza.click();
}







	
	
}