package com.yapo.entities;

public class DatosBuscarFal {

	String Texto;
	String Region;
	String Comuna; 
	String Calle;
	String Numero;
	
	public final String getTexto() {
		return Texto;
	}
	public final void setTexto(String texto) {
		Texto = texto;
	}
	public final String getRegion() {
		return Region;
	}
	public final void setRegion(String region) {
		Region = region;
	}
	public final String getComuna() {
		return Comuna;
	}
	public final void setComuna(String comuna) {
		Comuna = comuna;
	}
	public final String getCalle() {
		return Calle;
	}
	public final void setCalle(String calle) {
		Calle = calle;
	}
	public final String getNumero() {
		return Numero;
	}
	public final void setNumero(String numero) {
		Numero = numero;
	}
	
	
}
